# Welcome to your Quiz Question Expo app 👋


## Infomation 
Implement application by using React-native with Typescript with following requirement below.

- Display 20 questions each question has 4 answer
- Every application re-load or re-open will be random question and answer
- Display to Leader board
- publish on git repository


This is an [Expo](https://expo.dev) project created with [`create-expo-app`](https://www.npmjs.com/package/create-expo-app).

## Get started

1. Install dependencies

   ```bash
   npm install
   ```

2. Start the app

   ```bash
    npx expo start
   ```

In the output, you'll find options to open the app in a

- [development build](https://docs.expo.dev/develop/development-builds/introduction/)
- [Android emulator](https://docs.expo.dev/workflow/android-studio-emulator/)
- [iOS simulator](https://docs.expo.dev/workflow/ios-simulator/)
- [Expo Go](https://expo.dev/go), a limited sandbox for trying out app development with Expo

## Get a fresh project

When you're ready, run:

```bash
npm run reset-project
```


### Ref
1. <https://github.com/tiaeastwood/quiz-quest>
2. <https://docs.expo.dev/>
3. <https://stackoverflow.com/questions/58675179/error-emfile-too-many-open-files-react-native-cli>
4. <https://reactnative.dev/docs/components-and-apis>
5. <https://opentdb.com/>
