import React from "react";
import {
  Text,
  Pressable,
  Dimensions,
  StyleSheet,
  ViewStyle,
  TextStyle,
  DimensionValue,
  TouchableOpacity,
  TouchableHighlight,
} from "react-native";
import { Colors } from "@/constants/Colors";

interface ScreenDimensions {
  width: number;
  height: number;
}

interface Styles {
  button: ViewStyle;
  buttonText: TextStyle;
}

const getStyles = (screenDimensions: ScreenDimensions): Styles => {
  const isTablet = screenDimensions.width > 1000;

  const styles = StyleSheet.create({
    button: {
      justifyContent: "center",
      alignItems: "center",
      padding: 10,
      borderRadius: 20,
      margin: 5,
    },
    buttonText: {
      fontWeight: "bold",
      textTransform: "uppercase",
      fontSize: isTablet ? 20 : 14,
    },
  });
  return styles;
};

interface CustomButtonProps {
  fullWidth?: boolean;
  buttonText: string;
  width?: DimensionValue;
  disabled?: boolean;
  type: keyof typeof Colors.palette;
  onPress: () => void;
}

export const CustomButton: React.FC<CustomButtonProps> = ({
  fullWidth = false,
  buttonText,
  width,
  disabled = false,
  type,
  onPress,
}) => {
  const screenDimensions = Dimensions.get("screen");
  const styles = getStyles(screenDimensions);

  const dynamicStyles: ViewStyle = {
    width,
    backgroundColor: disabled ? "transparent" : Colors.palette[type],
    borderWidth: 2,
    borderColor: Colors.palette[type],
    alignSelf: fullWidth ? "stretch" : "center",
  };

  return (
    <TouchableHighlight
      style={[styles.button, dynamicStyles]}
      onPress={onPress}
      disabled={disabled}
    >
      <Text
        style={[
          styles.buttonText,
          { color: disabled ? Colors.palette[type] : "white" },
        ]}
      >
        {buttonText}
      </Text>
    </TouchableHighlight>
  );
};
