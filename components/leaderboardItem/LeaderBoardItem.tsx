import { Colors } from "@/constants/Colors";
import {
  Text,
  SafeAreaView,
  View,
  Dimensions,
  StyleSheet,
  ActivityIndicator,
} from "react-native";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
    paddingHorizontal: 20,
  },
  title: {
    fontSize: 24,
    fontWeight: "bold",
    marginBottom: 20,
  },
  listContainer: {
    flexGrow: 1,
    width: "100%",
  },
  itemContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingVertical: 10,
    borderBottomWidth: 1,
    borderBottomColor: "#ccc",
    width: "100%",
  },
  rank: {
    fontSize: 18,
    fontWeight: "bold",
    marginRight: 10,
    color: Colors.palette.background,
  },
  name: {
    flex: 1,
    fontSize: 18,
    marginLeft: 10,
    color: Colors.palette.background,
  },
  score: {
    fontSize: 18,
    fontWeight: "bold",
    marginRight: 10,
    color: Colors.palette.background,
  },
});

export const LeaderboardItem = ({
  item,
  index,
}: {
  item: any;
  index: number;
}) => {
  return (
    <View style={styles.itemContainer}>
      <Text style={styles.rank}>{index + 1}</Text>
      <Text style={styles.name}>{item.name}</Text>
      <Text style={styles.score}>{item.score}</Text>
    </View>
  );
};
