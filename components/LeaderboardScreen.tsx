import React, { useEffect, useState, useRef } from "react";
import {
  Text,
  SafeAreaView,
  FlatList,
  View,
  ActivityIndicator,
  Dimensions,
  StyleSheet,
} from "react-native";
import { useQuizContext } from "../context/QuizContext";
import { CustomButton } from "./customButton/CustomButton";
import { GradientWrapper } from "./gradientWrapper/GradientWrapper";
import { Colors } from "@/constants/Colors";
import { LeaderboardItem } from "./leaderboardItem/LeaderBoardItem";
import { router } from "expo-router";

interface ScreenDimensions {
  width: number;
  height: number;
}

const maxScore = 20; // Maximum score allowed

const mockLeaderboardData = [
  { name: "John Doe", score: 18 },
  { name: "Jane Smith", score: 20 },
  { name: "Mike Johnson", score: 15 },
  // Add more mock data as needed
];

const adjustScore = (score: number) => Math.min(score, maxScore);

const getStyles = (screenDimensions: ScreenDimensions) => {
  const isTablet = screenDimensions.width > 1000;

  return StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center",
      backgroundColor: "transparent",
    },
    subtitle: {
      fontWeight: "bold",
      fontSize: isTablet ? 30 : 20,
      color: Colors.palette.background,
      marginBottom: 5,
    },
    endTitle: {
      fontWeight: "bold",
      fontSize: isTablet ? 40 : 20,
      color: Colors.palette.background,
    },
    scoreAnnouncement: {
      fontWeight: "bold",
      fontSize: isTablet ? 60 : 30,
      color: Colors.palette.offWhite,
    },
    awardImg: {
      width: 150,
      height: 200,
      resizeMode: "contain",
    },
    bold: {
      fontWeight: "bold",
    },
    resultContainer: {
      flex: 1,
      marginTop: 20,
      alignItems: "center",
      width: "100%",
    },
    listContainer: {
      flex: 1,
      marginTop: 10,
      width: "100%",
      padding: 20,
      marginBottom: -40,
    },
    reviewAnswer: {
      fontSize: isTablet ? 20 : 14,
      marginVertical: 5,
    },
    buttonContainer: {
      flexDirection: "row",
      justifyContent: "space-between",
      alignItems: "center",
    },
  });
};

const Leaderboard: React.FC = () => {
  const { quizQuestions, recordedAnswers, reset } = useQuizContext();
  const [finalScore, setFinalScore] = useState<number | null>(null);
  const [leaderboardDataWithFinalScore, setLeaderboardDataWithFinalScore] =
    useState<
      {
        name: string;
        score: number;
      }[]
    >([]);
  const screenDimensions = Dimensions.get("screen");
  const styles = getStyles(screenDimensions);

  const getScore = () => {
    let score = 0;
    for (let i = 0; i < quizQuestions.length; i++) {
      if (quizQuestions[i].correctAnswer === recordedAnswers[i].answer) {
        score++;
      }
    }
    setFinalScore(score);
    const adjustedFinalScore = adjustScore(score);
    const updatedData = [
      ...mockLeaderboardData,
      { name: "You", score: adjustedFinalScore },
    ];
    updatedData.sort((a, b) => adjustScore(b.score) - adjustScore(a.score));
    setLeaderboardDataWithFinalScore(updatedData);
  };

  const playAgain = () => {
    reset("answers");
    router.push("quiz");
  };

  const quit = () => {
    reset("all");
    router.push('/')
  };

  useEffect(() => {
    getScore();
  }, []);

  // Sort by score in descending order and adjust scores if necessary
  leaderboardDataWithFinalScore.sort(
    (a, b) => adjustScore(b.score) - adjustScore(a.score)
  );

  if (finalScore === null) return <ActivityIndicator />;

  return (
    <GradientWrapper>
      <SafeAreaView style={styles.container}>
        <View style={styles.resultContainer}>
          <Text style={styles.endTitle}>All questions answered!</Text>

          <Text style={styles.scoreAnnouncement}>
            You scored {finalScore} out of {quizQuestions.length}
          </Text>

          <View style={styles.buttonContainer}>
            <CustomButton
              buttonText="Play Again"
              onPress={() => playAgain()}
              type="primary"
            />
            <CustomButton
              buttonText="Back"
              onPress={() => quit()}
              type="secondary"
            />
          </View>

          <View style={styles.listContainer}>
            <Text style={styles.subtitle}>Leader Board:</Text>
            <FlatList
              data={leaderboardDataWithFinalScore}
              renderItem={LeaderboardItem}
              keyExtractor={(item, index) => `${item.name}-${index}`}
            />
          </View>
        </View>
      </SafeAreaView>
    </GradientWrapper>
  );
};

export default Leaderboard;
