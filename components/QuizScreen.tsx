import React, { useEffect, useState } from "react";
import {
  Text,
  SafeAreaView,
  View,
  Dimensions,
  StyleSheet,
  ActivityIndicator,
} from "react-native";
import { CustomButton } from "./customButton/CustomButton";
import { GradientWrapper } from "./gradientWrapper/GradientWrapper";

import { useQuizContext } from "../context/QuizContext";
import { Colors } from "@/constants/Colors";
import { getSpecificNumberOfRegularQuestions } from "../api/quiz";
import { useNavigation } from "@react-navigation/native";
import { router } from "expo-router";

interface ScreenDimensions {
  width: number;
  height: number;
}

const getStyles = (screenDimensions: ScreenDimensions) => {
  const isTablet = screenDimensions.width > 1000;

  return StyleSheet.create({
    mainContainer: {
      flex: 1,
      height: "100%",
      width: "100%",
      justifyContent: "center",
      alignItems: "center",
      backgroundColor: "transparent",
    },
    gameContainer: {
      width: "80%",
      height: "75%",
      alignItems: "center",
      justifyContent: "space-evenly",
      paddingHorizontal: 20,
      backgroundColor: "rgba(255, 255, 255, 0.9)",
      borderRadius: 20,
    },
    title: {
      fontWeight: "bold",
      fontSize: isTablet ? 50 : 24,
      color: Colors.palette.primary,
    },
    question: {
      fontSize: isTablet ? 28 : 16,
      color: Colors.palette.offBlack,
    },
    buttonContainer: {
      justifyContent: "center",
      alignItems: "center",
      width: "100%",
    },
  });
};

const Quiz: React.FC = () => {
  const {
    quizQuestions,
    numQuestions,
    updateQuestions,
    addAnswer,
    counter,
    updateCounter,
  } = useQuizContext();

  const screenDimensions = Dimensions.get("screen");
  const styles = getStyles(screenDimensions);

  const endOfQuestions = counter === numQuestions - 1;

  const fetchQuestions = async () => {
    const regularQuestions = await getSpecificNumberOfRegularQuestions(
      numQuestions
    );
    await updateQuestions(regularQuestions);
  };

  const saveAnswer = (answer: string) => {
    addAnswer({ id: counter, answer });

    if (!endOfQuestions) {
      goToNextQuestion();
    } else {
      router.push("leaderBoard");
    }
  };

  const goToNextQuestion = () => {
    const nextQuestion = counter + 1;
    updateCounter(nextQuestion);
  };

  useEffect(() => {
    fetchQuestions();
  }, []);

  if (quizQuestions.length === 0) {
    return (
      <SafeAreaView style={styles.mainContainer}>
        <ActivityIndicator />
      </SafeAreaView>
    );
  } else {
    return (
      <GradientWrapper>
        <SafeAreaView style={styles.mainContainer}>
          <View style={styles.gameContainer}>
            <Text style={styles.title}>Question {counter + 1}</Text>
            <Text style={styles.question}>
              {quizQuestions && quizQuestions[counter].question}
            </Text>
            <View style={styles.buttonContainer}>
              {quizQuestions &&
                quizQuestions[counter].answers.map(
                  (answer: string, index: number) => (
                    <CustomButton
                      key={`${answer}-${index}`}
                      buttonText={answer}
                      onPress={() => saveAnswer(answer)}
                      type="primary"
                      fullWidth
                    />
                  )
                )}
            </View>
          </View>
        </SafeAreaView>
      </GradientWrapper>
    );
  }
};

export default Quiz;
