import React from "react";
import { View, StyleSheet, ViewStyle } from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import { Colors } from "@/constants/Colors";

interface GradientWrapperProps {
  children: React.ReactNode;
}

export const GradientWrapper: React.FC<GradientWrapperProps> = ({
  children,
}) => {
  return (
    <View style={styles.wrapper}>
      <LinearGradient
        colors={[Colors.palette.primary, Colors.palette.accent]}
        start={{ x: 0, y: 1 }}
        end={{ x: 0, y: 0.33 }}
        style={{ flex: 1 }}
      >
        {children}
      </LinearGradient>
    </View>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
  },
});
