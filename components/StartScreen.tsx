import React from "react";
import {
  Text,
  SafeAreaView,
  View,
  Dimensions,
  StyleSheet,
  ImageStyle,
  ViewStyle,
  TextStyle,
} from "react-native";
import { CustomButton } from "./customButton/CustomButton";
import { Colors } from "@/constants/Colors";
import Icon from "react-native-vector-icons/FontAwesome";
import { router } from "expo-router";
import { useQuizContext } from "../context/QuizContext";

interface ScreenDimensions {
  width: number;
  height: number;
}

interface Styles {
  container: ViewStyle;
  innerContainer: ViewStyle;
  outerOptionsContainer: ViewStyle;
  titleText: TextStyle;
  subtitle: TextStyle;
  optionsContainer: ViewStyle;
  homeImage: ImageStyle;
  input: ViewStyle;
  buttonsContainer: ViewStyle;
  sliderContainer: ViewStyle;
  slider: ViewStyle;
  questionType: ViewStyle;
}

const getStyles = (screenDimensions: ScreenDimensions): Styles => {
  const isTablet = screenDimensions.width > 1000;

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center",
      backgroundColor: Colors.palette.background,
    },
    innerContainer: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center",
      width: "100%",
    },
    outerOptionsContainer: {
      width: isTablet ? "70%" : "100%",
      alignItems: "center",
      marginTop: 20,
    },
    titleText: {
      fontSize: isTablet ? 140 : 80,
      fontWeight: "bold",
      fontFamily: "Blaka_400Regular",
      color: Colors.palette.primary,
    },
    subtitle: {
      fontSize: isTablet ? 20 : 14,
      fontWeight: "bold",
      marginVertical: 10,
      color: Colors.palette.primary,
    },
    optionsContainer: {
      width: isTablet ? "60%" : "100%",
      justifyContent: "center",
      alignItems: "center",
      marginVertical: isTablet ? 10 : 1,
    },
    homeImage: {
      height: isTablet ? "75%" : 400,
      width: "100%",
      position: "absolute",
      bottom: isTablet ? -80 : -10,
    },
    input: {
      height: 40,
      margin: 12,
      borderWidth: 1,
      padding: 10,
      borderRadius: 20,
      backgroundColor: Colors.palette.offWhite,
      color: "black",
    },
    buttonsContainer: {
      flexDirection: "row",
      justifyContent: "space-between",
      marginBottom: isTablet ? 5 : 20,
      marginLeft: isTablet ? 110 : 0,
    },
    sliderContainer: {
      flexDirection: "row",
      justifyContent: "center",
      marginBottom: 5,
    },
    slider: {
      width: "90%",
      height: 40,
    },
    questionType: {
      flexDirection: isTablet ? "column" : "row",
    },
  });

  return styles;
};

const Starter: React.FC = () => {
  const screenDimensions = Dimensions.get("screen");
  const styles = getStyles(screenDimensions);
  const { updateNumQuestions } = useQuizContext();
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.innerContainer}>
        <View style={{ alignItems: "center", justifyContent: "center" }}>
          <Icon
            name="question-circle"
            size={80}
            color={Colors.palette.primary}
          />
          <Text style={styles.subtitle}>Welcome to the Quiz!</Text>
        </View>

        <View style={styles.outerOptionsContainer}>
          <CustomButton
            width="80%"
            buttonText="Start"
            onPress={() => {
              updateNumQuestions(20);
              router.push("quiz");
            }}
            type="secondary"
          />
        </View>
      </View>
    </SafeAreaView>
  );
};

export default Starter;
