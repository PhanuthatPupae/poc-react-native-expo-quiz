import axios from 'axios';
import { decode } from 'html-entities';

interface QuestionData {
	questionNumber: number;
	question: string;
	answers: string[];
	correctAnswer: string;
}

export const getSpecificNumberOfRegularQuestions = async (
	numberOfQuestions: number,
): Promise<QuestionData[]> => {
	try {
		const response = await axios.get(
			`https://opentdb.com/api.php?amount=${numberOfQuestions}&type=multiple`,
		);

		const data = response.data;

		if (!data.results) {
			throw new Error('No results found');
		}

		return data.results.map((object: any, index: number) => ({
			questionNumber: index,
			question: decodeHtml(object.question),
			answers: [...object.incorrect_answers, object.correct_answer].sort(),
			correctAnswer: object.correct_answer,
		}));
	} catch (error) {
		console.error('Error fetching questions:', error);
		throw error; // Optionally handle or rethrow the error
	}
};

function decodeHtml(html: string): string {
	return decode(html);
}
