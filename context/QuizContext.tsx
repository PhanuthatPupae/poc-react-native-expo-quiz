// QuizContext.tsx

import React, { createContext, useContext, useReducer, ReactNode } from "react";
import { initialState, QuizState, quizReducer } from "./QuizReducer";

// Define context type
interface QuizContextType extends QuizState {
  updateQuestions: (fetchedQuestions: any[]) => void;
  addAnswer: (answer: any) => void;
  updateNumQuestions: (num: number) => void;
  updateQuestionType: (questionType: string) => void;
  updateCounter: (num: number) => void;
  reset: (option: "answers" | "quiz" | "all") => void;
}

// Define actions for the context
type QuizAction =
  | { type: "UPDATE_QUESTIONS"; payload: { quizQuestions: any[] } }
  | { type: "ADD_ANSWER"; payload: { recordedAnswers: any[] } }
  | { type: "UPDATE_NUM_QUESTIONS"; payload: { numQuestions: number } }
  | { type: "UPDATE_QUESTION_TYPE"; payload: { questionType: string } }
  | { type: "UPDATE_COUNTER"; payload: { counter: number } }
  | {
      type: "RESET_ANSWERS";
      payload: { recordedAnswers: any[]; counter: number };
    }
  | {
      type: "RESET_QUIZ";
      payload: {
        quizQuestions: any[];
        recordedAnswers: any[];
        numQuestions: number;
        questionType: string;
        counter: number;
      };
    };

// Create context
const QuizContext = createContext<QuizContextType | undefined>(undefined);

// Provider component
export const QuizProvider: React.FC<{ children: ReactNode }> = ({
  children,
}) => {
  const [state, dispatch] = useReducer(quizReducer, initialState);

  const updateQuestions = (fetchedQuestions: any[]) => {
    dispatch({
      type: "UPDATE_QUESTIONS",
      payload: {
        quizQuestions: fetchedQuestions,
      },
    });
  };

  const addAnswer = (answer: any) => {
    const updatedAnswerArray = state.recordedAnswers.concat(answer);
    dispatch({
      type: "ADD_ANSWER",
      payload: {
        recordedAnswers: updatedAnswerArray,
      },
    });
  };

  const updateNumQuestions = (num: number) => {
    dispatch({
      type: "UPDATE_NUM_QUESTIONS",
      payload: {
        numQuestions: num,
      },
    });
  };

  const updateQuestionType = (questionType: string) => {
    dispatch({
      type: "UPDATE_QUESTION_TYPE",
      payload: {
        questionType: questionType,
      },
    });
  };

  const updateCounter = (num: number) => {
    dispatch({
      type: "UPDATE_COUNTER",
      payload: {
        counter: num,
      },
    });
  };

  const reset = (option: "answers" | "quiz" | "all") => {
    if (option === "answers") {
      dispatch({
        type: "RESET_ANSWERS",
        payload: {
          recordedAnswers: initialState.recordedAnswers,
          counter: initialState.counter,
        },
      });
    } else {
      dispatch({
        type: "RESET_QUIZ",
        payload: {
          ...initialState,
        },
      });
    }
  };

  const value: QuizContextType = {
    ...state,
    updateQuestions,
    addAnswer,
    updateNumQuestions,
    updateQuestionType,
    updateCounter,
    reset,
  };

  return <QuizContext.Provider value={value}>{children}</QuizContext.Provider>;
};

// Custom hook to use the quiz context
export const useQuizContext = (): QuizContextType => {
  const context = useContext(QuizContext);

  if (!context) {
    throw new Error("useQuizContext must be used within a QuizProvider");
  }

  return context;
};
