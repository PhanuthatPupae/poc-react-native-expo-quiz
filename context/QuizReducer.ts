// Define types for state and actions
export interface QuizState {
  quizQuestions: QuizQuestion[];
  recordedAnswers: any[]; // Replace with actual type if needed
  numQuestions: number;
  questionType: string; // Replace with actual type if needed
  counter: number;
  quizResults: QuizResult[]; // Define quizResults in state
}

export interface QuizQuestion {
  questionNumber: number;
  question: string;
  answers: string[];
  correctAnswer: string;
}

export interface QuizResult {
  questionNumber: number;
  selectedAnswer: string;
  isCorrect: boolean;
}

// Define actions
export type QuizAction =
  | { type: "UPDATE_QUESTIONS"; payload: { quizQuestions: QuizQuestion[] } }
  | { type: "ADD_ANSWER"; payload: { recordedAnswers: any[] } }
  | { type: "UPDATE_NUM_QUESTIONS"; payload: { numQuestions: number } }
  | { type: "UPDATE_QUESTION_TYPE"; payload: { questionType: string } }
  | { type: "UPDATE_COUNTER"; payload: { counter: number } }
  | { type: "RESET_ANSWERS"; payload: { recordedAnswers: any[]; counter: number } }
  | { type: "RESET_QUIZ"; payload: QuizState }
  | { type: "UPDATE_QUIZ_RESULTS"; payload: { quizResults: QuizResult[] } };

// Initial state
export const initialState: QuizState = {
  quizQuestions: [],
  recordedAnswers: [],
  numQuestions: 20,
  questionType: "",
  counter: 0,
  quizResults: [],
};

// Reducer function
export const quizReducer = (
  state: QuizState = initialState,
  action: QuizAction
): QuizState => {
  switch (action.type) {
    case "UPDATE_QUESTIONS":
      return { ...state, quizQuestions: action.payload.quizQuestions };
    case "ADD_ANSWER":
      return { ...state, recordedAnswers: action.payload.recordedAnswers };
    case "UPDATE_NUM_QUESTIONS":
      return { ...state, numQuestions: action.payload.numQuestions };
    case "UPDATE_QUESTION_TYPE":
      return { ...state, questionType: action.payload.questionType };
    case "UPDATE_COUNTER":
      return { ...state, counter: action.payload.counter };
    case "RESET_ANSWERS":
      return {
        ...state,
        recordedAnswers: action.payload.recordedAnswers,
        counter: action.payload.counter,
      };
    case "RESET_QUIZ":
      return {
        ...initialState,
        quizQuestions: action.payload.quizQuestions,
        recordedAnswers: action.payload.recordedAnswers,
        numQuestions: action.payload.numQuestions,
        questionType: action.payload.questionType,
        counter: action.payload.counter,
      };
    case "UPDATE_QUIZ_RESULTS":
      return { ...state, quizResults: action.payload.quizResults };
    default:
      return state;
  }
};
